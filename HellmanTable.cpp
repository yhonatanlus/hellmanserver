#include "HellmanTable.h"
// #include <urcu.h>
#include <limits>
#include <sstream>
#include <iostream>
#include <unistd.h>
#include <string> 
#include <arpa/inet.h>

#define MT_REMOVE_TABLE 0
#define MT_APPEND_TABLE 1

// CONFIGURATION // 
#define DEBUG_PRINTS 0
#define PROCESS_PRINTS 0

#define START_FROM_HEAD_TABLE 0
#define START_FROM_TAIL_TABLE 0

size_t TOTAL_MEM = 1000 * 1024; // In B (Default size)
size_t USED_MEM = 0; // In B

using namespace RaaS; 
using std::cout;
using std::string;
using std::endl;
using ul = unsigned long long;

const hash_t HellmanTable::DEF_EMPTY_FLAVOR = -1;
const len_t HellmanTable::DEF_NO_INDEX = -1;


void HellmanTable::mt_handle_tables(const string FileName){

	ifstream in(sFilesPath + "/" + FileName, ios::binary | ios::in);

	if (in.good())
	{
		MapsPool maps_pool;
		chain_map* temp_map;
		
		SubTable* prev_table = NULL;
		SubTable* curr_table = NULL;
		
		value_type *data = NULL;
        hash_t flavor = 0;
		hash_t mt_tail_table_flavor = -1;
        len_t len = 0;
        count_t data_size = 0; 
		int table_size = 0, last_table_size = 0; // In KB
		int avilable_mem = 0; // In KB
		
		while (true) {
			// Reading the tables.bin file
			if (!in.read((char*)&flavor, sizeof(flavor))){
					// EOF reached
					in.clear();
					in.seekg(0, ios::beg);
					in.read((char*)&flavor, sizeof(flavor));
			}
#if(DEBUG_PRINTS)
	cout << "mt_handle_tables: loaded table flavor is: " << flavor << endl << endl;
#endif
			// Read a table
			in.read((char*)&len, sizeof(len));
			in.read((char*)&data_size, sizeof(data_size));
			if (data == NULL){
				data = new value_type [data_size]; 
			}
			in.read((char*)data, data_size*8);
			
			// All tables.bin file is in memory
			if(mt_tail_table_flavor == flavor){
				curr_table->mt_update_next_table(mt_tail_table);
				mt_wait_for_new_mem();
				curr_table->mt_update_next_table(NULL);
			}
			
			table_size = data_size * 8; 
			avilable_mem = mt_get_avilable_mem();
			while (avilable_mem < table_size){
				
#if(DEBUG_PRINTS)
				cout << "mt_handle_tables: avilable_mem is: " << avilable_mem << " bytes"<< endl;
				cout << "mt_handle_tables: Total table amount is:" << mt_total_tables << " tables" << endl;
#endif
				// A table can be removed
				if(mt_total_tables != 0){
					last_table_size = (8 * mt_tail_table->getNumOfChains());  // Get size of the last table in bytes
#if(DEBUG_PRINTS)	
					cout << "mt_handle_tables: Removing table: flavor: " << mt_tail_table->getFlavor() <<  " || size:" << last_table_size << " || chain amount: " << mt_tail_table->getNumOfChains() << endl;
#endif	
					mt_delete_list_tail(&maps_pool);
					mt_tail_table_flavor = mt_tail_table->getFlavor();
#if(DEBUG_PRINTS)
					cout << "mt_handle_tables: Table was removed" << endl;
#endif					
					avilable_mem = mt_update_used_mem(last_table_size, MT_REMOVE_TABLE);	
				}
				else {
					avilable_mem = mt_get_avilable_mem();
				}
			}
			/* Adding a new table to the list */
			temp_map = maps_pool.get(table_size/8); // Get empty map from the pool
			curr_table = new SubTable(len, flavor, temp_map); // Create a new table instance
			curr_table->load(data, data_size); // Load data into it
			mt_update_list_head(curr_table); // Put it in the list
			
			if(prev_table != NULL){
				prev_table->mt_update_next_table(curr_table);
			}
			if(mt_tail_table == NULL){
				mt_tail_table = curr_table;
				mt_tail_table_flavor = flavor;
			}
			
			prev_table = curr_table;
			mt_total_tables++;			
			table_size = 8 * curr_table->getNumOfChains(); // Since this is an unordered map - in case we have 2 same values one is being ignored
			avilable_mem = mt_update_used_mem(table_size, MT_APPEND_TABLE);	
			
#if(DEBUG_PRINTS)
			cout << "mt_handle_tables: Adding table: flavor: " << flavor << " || size: "<< table_size << " || chain amount: " << curr_table->getNumOfChains() << "|| total tables: " << mt_total_tables << endl;
			cout << "mt_handle_tables: avilable mem is:" << avilable_mem << " bytes" << endl;
#endif	
		}
		delete data;
	}
    else{ 	
        cout << "Error openning tables file" << endl;
    }
	in.close();
}


int HellmanTable::mt_get_avilable_mem(){
	int avilable_mem;
	
	pthread_mutex_lock(&mt_mem_mu);
	avilable_mem = TOTAL_MEM - USED_MEM;
	pthread_mutex_unlock(&mt_mem_mu);
	
	if(avilable_mem > 0)
		return avilable_mem;
	else
		return 0;
}


int HellmanTable::mt_update_used_mem(int table_size, bool load_type ){
	int avilable_mem;
	
	pthread_mutex_lock(&mt_mem_mu);
	if (load_type == MT_APPEND_TABLE) {
		USED_MEM += table_size;
	}
	else{
		USED_MEM -= table_size;
	}	
	avilable_mem = TOTAL_MEM - USED_MEM;
	pthread_mutex_unlock(&mt_mem_mu);
	
	return avilable_mem;
}

bool HellmanTable::mt_update_new_mem(int new_mem){

#if(DEBUG_PRINTS)
	cout << "Changing memory to: " << new_mem * 1024 << " Bytes" << endl;
#endif	
	pthread_mutex_lock(&mt_mem_mu);
	TOTAL_MEM = new_mem * 1024;
	pthread_cond_signal(&mt_new_mem);
	pthread_mutex_unlock(&mt_mem_mu);
	
	return true;
}

void HellmanTable::mt_wait_for_new_mem(){
	
	pthread_mutex_lock(&mt_mem_mu);
	while(TOTAL_MEM >= USED_MEM){
		pthread_cond_wait(&mt_new_mem, &mt_mem_mu);
	}
	pthread_mutex_unlock(&mt_mem_mu);
	
}


void HellmanTable::mt_update_list_head(SubTable* table ){
	pthread_mutex_lock(&mt_list_head_lock);
	
	mt_head_table = table;
	
	pthread_mutex_unlock(&mt_list_head_lock);
}


SubTable* HellmanTable::mt_get_list_head(){
	SubTable* head_table;
	pthread_mutex_lock(&mt_list_head_lock);
	
	head_table = mt_head_table;
	head_table->mt_table_read_lock();
	
	pthread_mutex_unlock(&mt_list_head_lock);
	return head_table;
}

void HellmanTable::mt_delete_list_tail(MapsPool* maps_pool){
	SubTable* next_tail_table;
	
	next_tail_table = mt_tail_table->mt_get_next_table();
	pthread_mutex_lock(&mt_list_tail_lock);
	mt_tail_table->mt_table_write_lock();							 // Table write lock 
	maps_pool->release(mt_tail_table->getMap());
	delete mt_tail_table;											 // Pop the last element in the back and call it's destructor
	
	mt_total_tables--;
	mt_tail_table = next_tail_table;								 //set new tail table
	pthread_mutex_unlock(&mt_list_tail_lock);		
}

SubTable* HellmanTable::mt_get_list_tail(){
	SubTable* tail_table;
	pthread_mutex_lock(&mt_list_tail_lock);
	
	tail_table = mt_tail_table;
	tail_table->mt_table_read_lock();
	pthread_mutex_unlock(&mt_list_tail_lock);
	return tail_table;	
}


void HellmanTable::mt_get_preimages(int client_sock){
	hash_t ret, start_table_flavor, curr_table_flavor = -1;
	SubTable *curr_table = NULL, *next_table = NULL;
	
	hash_t query = 0, preimage = 0;
	char recv_buf[16];
	int bytes_recieved;
	int preimage_to_send = 0;	
	
	memset(recv_buf, 0x00, 16);
	if (START_FROM_HEAD_TABLE) {
		curr_table = mt_get_list_head();
	}
	if (START_FROM_TAIL_TABLE){
		curr_table = mt_get_list_tail();
	}
	
	// On each iteration we recieve a new query from the client
	while((bytes_recieved = read(client_sock, recv_buf, sizeof(recv_buf) - 1)) > 0)
	{	
		recv_buf[bytes_recieved] = '\0';
		query = atoi(recv_buf);
		preimage = 0;
		
#if(DEBUG_PRINTS)			
		cout << "mt_get_preimages: got new query: " << query << endl;
#endif		

		curr_table_flavor = -1;
		// START_FROM_HEAD_TABLE needs to be set to 1 in case we want it to start from the head table one each query
		// START_FROM_TAIL_TABLE needs to be set to 1 in case we want it to start from the tail table one each query
		if (next_table != NULL && (START_FROM_HEAD_TABLE || START_FROM_TAIL_TABLE))
		{	
			if (START_FROM_HEAD_TABLE) {
				curr_table->mt_table_read_unlock();
				curr_table = mt_get_list_head();
			}
			if (START_FROM_TAIL_TABLE){
				curr_table->mt_table_read_unlock();
				curr_table = mt_get_list_tail();
			}
		}
		start_table_flavor = curr_table->getFlavor();
		
		while(curr_table_flavor != start_table_flavor){
#if(DEBUG_PRINTS)		
			cout << "mt_get_preimages: trying to resolve table with flavor " << curr_table_flavor << endl; 
#endif			
			ret = curr_table->resolve(query);
#if(DEBUG_PRINTS)		
			cout << "mt_get_preimages: resolve result " << ret << endl;
#endif					
			// Set next table
			next_table = curr_table->mt_get_next_table();
			next_table->mt_table_read_lock();
			curr_table->mt_table_read_unlock();
			curr_table = next_table;
			// Read new table flavor
			curr_table_flavor = curr_table->getFlavor();
				
			// In case we found a preimage
			if(ret != SubTable::FAILURE){ 
				preimage = ret;					
// #if(DEBUG_PRINTS)			
				cout << "mt_get_preimages: Found PreImage for " << query << " : " << preimage << endl;	
// #endif						
				break;
			}
		}
		// In this case we iterated over all of the tables (preimage will be -1)
		preimage_to_send = htonl(preimage);
		write(client_sock, &preimage_to_send, sizeof(int));
	}
	
	curr_table->mt_table_read_unlock();
	cout << "Client Disconnected\n" << endl;
}

