#include "SubTable.h"
#include <limits>
#include <iostream> // to allow prints for prints-debug
#include <typeinfo> 
using namespace RaaS;

#define ALLOW_PRINTS 0


SubTable::SubTable(const len_t length, const hash_t flavor, chain_map* map) : m_flavor(flavor), m_chainLength(length), m_map(map){
    mt_table_rw_lock_init();
	is_has_next_table = false;
	next_table = NULL;
}

SubTable::~SubTable(){
	mt_table_write_unlock();
	pthread_cond_destroy(&table_rw_lock.write);
	pthread_cond_destroy(&table_rw_lock.next_table_avilable);
	pthread_mutex_destroy(&table_rw_lock.lock);
	pthread_mutex_destroy(&table_rw_lock.next_table_lock);
	m_map->clear();
}

void SubTable::mt_table_rw_lock_init(){
	table_rw_lock.readers = 0;
	pthread_mutex_init(&table_rw_lock.lock,NULL);
	pthread_mutex_init(&table_rw_lock.next_table_lock,NULL);
    pthread_cond_init(&table_rw_lock.write, NULL);
    pthread_cond_init(&table_rw_lock.next_table_avilable, NULL);
}

void SubTable::mt_table_read_lock(){
	pthread_mutex_lock(&table_rw_lock.lock);
	
	table_rw_lock.readers++;
	
	pthread_mutex_unlock(&table_rw_lock.lock);
}

void SubTable::mt_table_read_unlock(){
	pthread_mutex_lock(&table_rw_lock.lock);
	
	table_rw_lock.readers--;
	if(table_rw_lock.readers == 0)
		pthread_cond_signal(&table_rw_lock.write);
	
	pthread_mutex_unlock(&table_rw_lock.lock);
}

void SubTable::mt_table_write_lock(){
	pthread_mutex_lock(&table_rw_lock.lock);
	while(table_rw_lock.readers != 0)
		pthread_cond_wait(&table_rw_lock.write, &table_rw_lock.lock);
}

void SubTable::mt_table_write_unlock(){
	pthread_mutex_unlock(&table_rw_lock.lock);
}


void SubTable::mt_update_next_table(SubTable* table){
	pthread_mutex_lock(&table_rw_lock.next_table_lock);
	
	next_table = table;
	if (next_table != NULL){
		pthread_cond_broadcast(&table_rw_lock.next_table_avilable);
	}

	pthread_mutex_unlock(&table_rw_lock.next_table_lock);
}


SubTable* SubTable::mt_get_next_table(){
	SubTable* return_table;
	pthread_mutex_lock(&table_rw_lock.next_table_lock);
	
	if (next_table == NULL)
		pthread_cond_wait(&table_rw_lock.next_table_avilable, &table_rw_lock.next_table_lock);
	return_table = next_table;
	
	pthread_mutex_unlock(&table_rw_lock.next_table_lock);
	
	return return_table;
}
	

void SubTable::load(value_type data[], count_t data_len){
    m_map->clear();
    BLOCK_SIZE = data_len;
    for(count_t i = 0; i < BLOCK_SIZE; i++)
        m_map->insert(data[i]);

}


hash_t SubTable::resolve(hash_t query){
	chain_map::iterator it;

	hash_t preimage;
	hash_t pointer = query;

	for (uint32_t i = 0; i < this->m_chainLength; i++){
        it = m_map->find(pointer);
		if (it != m_map->end())
		{
#if(ALLOW_PRINTS)
			cout << "tbl: " << i << " " << m_flavor << " FOUND" << endl;
#endif
			preimage = it->second;
			preimage = n_hash(preimage, m_flavor, m_chainLength - i - 1);

			hash_t temp = preimage;
			temp = n_hash(temp, m_flavor, 1);

			if (temp == query)
			{
#if(ALLOW_PRINTS)
				cout << "tbl: " << i << " " << m_flavor << " FOREAL" << endl; // TODO REMOVE
#endif
				return (preimage ^ m_flavor);
			}
		}
#if(ALLOW_PRINTS)
		cout << "tbl: " << i << " " << m_flavor << " NOTFOUND" << endl; 
#endif
		pointer = n_hash(pointer, m_flavor, 1); // "advancing" by 1 hash(ptr)
	}
	return FAILURE;
}


