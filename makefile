CPPFLAG=-std=c++14 -O0 -g3 -Wall -pthread -c
LDFLAGS=-g -pthread
RES_FILE=HellmanServer
RM := rm -rf

all: main.o HellmanTable.o SubTable.o MD5.o
	@echo 'Remaking Executable'
	g++ $(LDFLAGS) -o $(RES_FILE) HellmanTable.o SubTable.o MD5.o main.o

MD5.o: MD5.h
	g++ $(CPPFLAG) MD5.c

SubTable.o: MD5.h SubTable.h SubTable.cpp
	g++ $(CPPFLAG) SubTable.cpp	

HellmanTable.o: SubTable.h HellmanTable.h HellmanTable.cpp
	g++ $(CPPFLAG) HellmanTable.cpp

main.o: HellmanTable.h main.cpp
	g++ $(CPPFLAG) main.cpp
