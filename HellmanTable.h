#ifndef HELLMAN_TABLE_H
#define HELLMAN_TABLE_H

#include "SubTable.h"

#include <mutex>
#include <condition_variable>
#include <list>

#include <iostream> 

using namespace std;

namespace RaaS {

    enum LoadType : uint8_t {
        Append = 1,
        Override = 2
    };

    //Dynemic size Hellman Table
    class HellmanTable
    {
        len_t m_chainLength;                //Expected length of chains
		std::string sFilesPath;
		list<SubTable> mt_m_tables;			//For multithreading purpose
		SubTable *mt_tail_table;
		SubTable *mt_head_table;
		size_t mt_total_tables;
		pthread_mutex_t mt_list_tail_lock;
		pthread_mutex_t mt_list_head_lock;
		pthread_cond_t mt_new_mem;
		pthread_mutex_t mt_mem_mu;


    public:
        /*->*/
        // might not be up-to-date if getPreimageMT was used, but we won't use it
		static const hash_t DEF_EMPTY_FLAVOR;// = -1;  // =4294967295, =FFFFFFFF (actual initialization within cpp)
		static const len_t DEF_NO_INDEX; // =-1 (actual initialization within cpp)
		
        /*
        Default Constructor : Initlizes the chain length
        @param chainLength : Initlize chain length value
        */
        HellmanTable(len_t chainLength, std::string filesPath) : m_chainLength(chainLength), \
                                                                         sFilesPath(filesPath), 
                                                                         mt_tail_table(NULL), 
			                                                             mt_head_table(NULL), mt_total_tables(0){
																			 pthread_mutex_init(&mt_list_head_lock,NULL);
																			 pthread_mutex_init(&mt_list_tail_lock,NULL);
																			 pthread_mutex_init(&mt_mem_mu,NULL);
																			 pthread_cond_init(&mt_new_mem, NULL);
																		 }

		// Updates the total memory in the system
		bool mt_update_new_mem(int new_mem);
		
		/* It is possible to change one day those functions the the memusage() function in main.cpp, meanwhile it works with "virtual" memory*/
		// waits for new mem event
		void mt_wait_for_new_mem();
		// Get the current avilable memory
		int mt_get_avilable_mem();
		// Update the current memory being used, returns the avilable memory
		int mt_update_used_mem(int table_size, bool load_type);
		
		// updates list head with a new table
		void mt_update_list_head(SubTable* table );
		
		// returns the list head table
		SubTable* mt_get_list_head();
		
		// deletes tail table
		void mt_delete_list_tail(MapsPool* maps_pool);
		
		// return the list tail table
		SubTable* mt_get_list_tail();
		
		// This function is responsible for the table loading and removing 
		void mt_handle_tables(const string FileName);
		
		// This function handles the communication of the host to the client
		void mt_get_preimages(int client_sock);
    };
}

#endif 