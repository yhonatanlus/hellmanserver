#ifndef SUB_TABLE_H
#define SUB_TABLE_H

#include <unordered_map>
#include <fstream>
#include <ext/malloc_allocator.h>
#include <iostream>
#include <mutex>
#include <malloc.h>

#include "MD5.h"


#define DEF_BLOCK_SIZE 1024 // when DEBUGGING, make sure to update
#define DEBUGGING_SUBTABLE 0


using namespace std;

namespace RaaS {

	// Manage the read and write from the tables. Multiple readers, single writer at a time
	struct TableRWLock{
		pthread_mutex_t lock;
		pthread_mutex_t next_table_lock;
		pthread_cond_t write;
		pthread_cond_t next_table_avilable;
		int readers; // amount of readers
	};

    //Chain representative type : chain end-point, chain start-point
    typedef pair<hash_t, hash_t> value_type;


    /*
    Hash table
    Key :     chain end-point (EP).
    Value :   chain start-point (SP).
    */
    typedef unordered_map<hash_t, hash_t, hash<hash_t>, equal_to<hash_t>> chain_map;


    //////////////////////////////////////////////////////////////////////////////////////////////////////

    //Hellman Table class implementation with constant size and unique flavor
    class SubTable
    {
        hash_t m_flavor;        // Flavor to distinguish the function from the other subtables
		len_t m_chainLength;    // Expected length of chains
		chain_map* m_map;        // Hash Table : end-point -> start-point
		
		bool is_has_next_table;
		SubTable* next_table;
		struct TableRWLock table_rw_lock;
		
		// when debugging - changes to '2'
#if(DEBUGGING_SUBTABLE)
		count_t BLOCK_SIZE = 3; //Number of chains per block //
#else
        count_t BLOCK_SIZE = DEF_BLOCK_SIZE; //Number of chains per block //
#endif

    public:
        static const hash_t FAILURE = 0;        //Preimage not found (equivalent to NULL)
                                                //Note : Since FAILURE value is 0, zero could not be a preimage value
		
		/* mt rw_lock functions */
		void mt_table_rw_lock_init();
		void mt_table_read_lock();
		void mt_table_read_unlock();
		void mt_table_write_lock();
		void mt_table_write_unlock();
		
		/* update next table avilable */
		void mt_update_next_table(SubTable* table);
		/* returns the next table */
		SubTable* mt_get_next_table();


        /*
        Default Constructor : Allocate buckets for hash table.
        @param length : The desired length of the chains.
        @param flavor : Distinguish falvor value.
        */
        SubTable(const len_t length, const hash_t flavor, chain_map* map);
		
		// Default Destructor
		~SubTable();
		

        /*
        Read BLOCK_SIZE chains from given data array, and build data structure.
        @param data : Array of chains, representred as pair <End-point, Start-point>
        */
        void load(value_type data[], count_t data_len);

        /*
        Compute the preimage of a given query.
        @param query : The query value to be resolved.
        Returns value of preimage on success, otherwise returns FAILURE.
        */
        hash_t resolve(hash_t query);


        const count_t getNumOfChains() const { return m_map->size();}

        const hash_t getFlavor() const { return m_flavor;}

        count_t getBlockSize() const { return BLOCK_SIZE;}

		chain_map* getMap() { return m_map;}
    };
	

	struct MapNode{
		chain_map* map;
		bool is_cleared;
		struct MapNode* previous_node;
	};
	
	class MapsPool{

	public:
		
		// Constructor
		MapsPool(){
			size = 0;
		};
		
		// Destructor
		~MapsPool(void){
			struct MapNode* temp_node;
			
			while(_maps != NULL){
				(_maps->map)->rehash(0);
				temp_node = _maps;
				_maps = _maps->previous_node;
				delete temp_node;
			}
		};
		
		// Get a map from the pool
		chain_map* get(size_t block_size){
			chain_map *temp_map;
			struct MapNode* temp_node = _maps;
			
			// In case the pool is empty, creates a new map
			if(size == 0){
				temp_map = new chain_map;
				temp_map->reserve(block_size);
			}
			// otherwise, take a map from the pool
			else{
				if(_maps->map == NULL)
				{
					_maps->map = new chain_map;
				}
				temp_map = _maps->map;
				_maps = _maps->previous_node;
				delete temp_node;
				size--;
			}
				return temp_map;
		};
		
		// Return a map to the pool
		bool release(chain_map* map){
			struct MapNode* new_node = new MapNode;
			new_node->is_cleared = false;
			
			if(new_node){
				new_node->previous_node = _maps;
				new_node->map = map;
				_maps = new_node;
				if(_maps->previous_node != NULL){
					if(_maps->previous_node->is_cleared == false){
						delete _maps->previous_node->map;
						_maps->previous_node->map = NULL;
						_maps->previous_node->is_cleared = true;
						malloc_trim(0);
					}
				}
				size++;
				return true;
			}
			else{
				return false;
			}
				
		};
	
	protected:
		struct MapNode* _maps = NULL;
		size_t size;
	};
}

#endif 
