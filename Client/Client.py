import socket, time, struct, json, logging
from threading import Thread, Lock, Event

class Client:
	# Init method...
	def __init__(self, ip_address, port, queries_file):
		self.connection = self.establish_connection(ip_address, port)
		if self.connection is None:
			print "Could not create new client"
			return None
		
		self.queries_file = queries_file
		self.is_alive = True
		
		if self.create_com_thread() is False:
			print "Could no create com thread"
			return None

				
	# Simple function, just as it sounds	
	def create_com_thread(self):
		try:
			com_thread = Thread(target = self.communicate, args = ())
			com_thread.daemon = True
			com_thread.start()
			return True
		except:
			return False

			
	# Create connection to the server
	def establish_connection(self, ip_address, port):
		try:
			server = (ip_address, port)
			sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			sock.connect(server)
			return sock
		except:
			print "Could not establish connection to server"
			return None
			
	
	# The communication thread to the server, this function perfroms the queries
	def communicate(self):
		self.connection.send("new_client\x00")
		status = self.connection.recv(16)
		if status == "OK":
			while self.is_alive is True:
				query_value = int(self.queries_file.readline())
				start_time = time.time()
				self.connection.send(str(query_value))
				result = self.connection.recv(128)
				if (result == ""):
					print "something went wrong with client"
				else:
					result_value = socket.ntohl(struct.unpack("I", result)[0])
					end_time = time.time()
					duration = end_time - start_time
					self.log_to_file(query_value, result_value, duration)
		else:
			print "something went wrong"
		
		# End communication
		self.connection.close()
			
			
			
	# This what kills the client
	def kill(self):
		self.is_alive = False
		
		
	def log_to_file(self, query, result, duration):
		logging.debug(json.dumps({"query" : str(query), \
									"result" : str(result), \
									"duration" : str(duration)}))