import os, Queue, argparse, socket, logging, json, time
from Client import Client

SERVER_IP = "127.0.0.1"
SERVER_PORT = 12345
LOG_FILE_NAME = "Log.log"
QUERIES_FILE_NAME = "queries.txt"
CLIENTS_POOL = None
CURRENT_MEMORY = 100

def benchmark(h_queries_file, benchmark_file_name):
	try:
		benchmark_file = open(benchmark_file_name, 'r')
	except:
		print "Could not open benchmark file"
		return
	
	while True:
		try:
			command_json = json.loads(file.readline(benchmark_file))
		except:
			break
		
		new_clients = command_json["Clients"]
		new_memory = command_json["Memory"]
		duration = command_json["Duration"]
		
		logging.info(json.dumps({"memory" : str(new_memory), \
									"clients" : str(new_clients), \
									"duration" : str(duration)}))
		set_client_amount(int(new_clients), h_queries_file, True)
		set_memory_size(int(new_memory), True)
		
		print "simulating " + str(new_clients) + " client with " + str(new_memory) + " kb for " + str(duration) + " seconds" 
		time.sleep(int(duration))
	
	benchmark_file.close()
	print "Benchmark Completed!"
			

# Creates a new client and adds it to pool
def new_client(h_queries_file):
	global CLIENTS_POOL
	
	new_client = Client(SERVER_IP, SERVER_PORT, h_queries_file)
	if new_client.connection is None:
		pass
	else:
		print new_client
		CLIENTS_POOL.put(new_client)
	

# Removes a client from the client pool
def rm_client():
	global CLIENTS_POOL
	if CLIENTS_POOL.empty():
		print "can not remove client"
	else:
		client = CLIENTS_POOL.get()
		client.kill()
	
# Changes the clients amount to the specified 	
def set_client_amount(amount, h_queries_file, is_benchmark):
	global CLIENTS_POOL
	global CURRENT_MEMORY
	
	current_clients_amount = CLIENTS_POOL.qsize()
	if amount > current_clients_amount:
		for i in range(amount - current_clients_amount):
			new_client(h_queries_file)
	elif amount < current_clients_amount:
		for i in range(current_clients_amount - amount):
			rm_client()
	else:
		pass
	
	if (is_benchmark is False):
		logging.info(json.dumps({"memory" : str(CURRENT_MEMORY), \
									"clients" : str(amount)}))

def set_memory_size(size, is_benchmark):
	global CLIENTS_POOL
	
	try:
		server = (SERVER_IP, SERVER_PORT)
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		sock.connect(server)
	except:
		print "Could not establish connection to server"
		return None
	
	try:
		sock.send("change_mem " + str(size))
		result = sock.recv(16)
		if result == "OK":
			print "Memory was changed to: " + str(size)
			
			global CURRENT_MEMORY
			CURRENT_MEMORY = size
		else:
			print "Could not change the memory"
		sock.close()
	except:
		print "Could not change the memory"
		sock.close()
	
	if (is_benchmark is False):
		logging.info(json.dumps({"memory" : str(size), \
							"clients" : str(CLIENTS_POOL.qsize())}))
	
	
# Load a table with name
def set_tables(name):
	print "Needs to be implemented"
	pass


	
# Print current application status
def print_info():
	global CLIENTS_POOL
	global CURRENT_MEMORY
	
	print "Details:\n" + \
          "Total Clients: " + str(CLIENTS_POOL.qsize()) + "\n" + \
		  "Current Memory: " + str(CURRENT_MEMORY) + " KB\n"
		  
	
			
# Main
if __name__ == "__main__":

	with open(LOG_FILE_NAME, 'w'):
		pass

	logging.basicConfig(filename='Log.log', format="%(levelname)s:%(message)s", level=logging.DEBUG, mode='w')
	
	# The clients pool is implemented as a FIFO queue
	CLIENTS_POOL = Queue.Queue()
	
	h_queries_file = open(QUERIES_FILE_NAME,'r')

	print "Welcome to HellmanTables application, type \"help\" to view possible commands"
	while(True):
		command = raw_input("Command << ")
		command = command.split(" ")
		if command[0] == "help":
			print "new_client            -  Create a new client which will perform queries\n" + \
				"rm_client             -  Remove one client\n" + \
				"set_client <amount>   -  Set the clients amount to be the specified amount\n" + \
				"load_tables <name>    -  Loads tables file with name (This operation replaces the exiting one)\n" + \
				"change_mem <size(KB)> -  Sets RAM memory to size\n" + \
				"benchmark <file path> -  Executes a benchmark according to the benchmark file privded\n" + \
				"info                  -  print info about the current application status\n" + \
				"exit or quit          -  exit the application\n"
		elif command[0] == "new_client":
			new_client(h_queries_file)
		elif command[0] == "rm_client":
			rm_client()
		elif command[0] == "set_client":
			try:
				amount = int(str(command[1]))
				set_client_amount(amount, h_queries_file, False)
			except:
				print "Bad or no amount is given\n"				
			

		elif command[0] == "load_table":
			try:
				amount = int(command[1])
				set_tables(name)
			except:
				print "Bad or no amount is given\n"	
		elif command[0] == "change_mem":
			try:
				size = int(command[1])
				set_memory_size(size, False)
			except:
				print "Bad or no size is given\n"						
		elif command[0] == "benchmark":
			if (len(command) < 2):
				print "Please provide a benchmark file name"
			else:
				benchmark(h_queries_file, command[1])
		elif command[0] == "info":
			print_info()
		elif command[0] == "exit":
			break	
		elif command[0] == "quit":
			break
		else:
			print "Unkown command type \"help\" to get the possible commands\n"
			
	exit()
	
	
