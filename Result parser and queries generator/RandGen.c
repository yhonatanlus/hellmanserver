// This file creates the queries to be queired by the clients
#include <stdio.h>
#include <stdlib.h>

int main() {
	
	// Query amount - change this value.
	int queryAmount = 1000000;

	// Set seed
	srand(0x12345678);
	int i = 0;
	
	// Open the file
	FILE *f = fopen("queries.txt", "w");
	if (f == NULL)
	{
		printf("Error");
		exit(1);
	}
	
	// Write queries
	for( i = 0; i < queryAmount; i++)
	{
		fprintf(f,"%d\n", rand());
	}
	printf("Done :) \n");
}