import csv, sys, json

# Main
if __name__ == "__main__":
	# Open log file
	try:
		if (len(sys.argv) > 1):
			log_file = open(sys.argv[1], 'r')
		else:
			log_file = open("Log.log", 'r')
	except:
		print "error opening log file"
		sys.exit(0)
	
	# Open results file
	try:
		if (len(sys.argv) > 2):
			results_file = open(sys.argv[2], 'w')
		else:
			results_file = open("results.csv", 'w')
	except:
		print "error opening results file"
		sys.exit(0)
	
	writer = csv.writer(results_file)
	writer.writerow(["Memory", "Number of clients", "Duration simulated", "Total queries", "Successful queries", "Average query duration", "Average bad query duration", "Queries per second"])
	
	state = {"memory" : 0 , "clients" : 0, "duration" : 0}
		
	total_queries = 0
	successful_queries = 0
	total_duration = 0
	total_bad_queries_duration = 0
	
	line = log_file.readline()
	while line != "":
		if line[0:4] == "INFO":
			#first log last state
			if (int(state["duration"]) != 0):
				writer.writerow([state["memory"],state["clients"], state["duration"], str(total_queries), str(successful_queries), str(float(total_duration) / float(total_queries)), str(float(total_bad_queries_duration)/float(total_queries - successful_queries)), str(float(total_queries)/float(state["duration"]))])
			info_json = json.loads(line[5:])
			state["memory"] = info_json ["memory"]
			state["clients"] = info_json["clients"]
			state["duration"] = info_json["duration"]
			total_queries = 0
			successful_queries = 0
			total_duration = 0
			total_bad_queries_duration = 0
		else:
			debug_json = json.loads(line[6:])
			total_queries += 1
			if (int(debug_json["result"]) != 0):
				successful_queries += 1
			else:
				total_bad_queries_duration += float(debug_json["duration"])
			total_duration += float(debug_json["duration"])
			
		line = log_file.readline()

	if (int(state["duration"]) != 0):
		writer.writerow([str(state["memory"]),state["clients"], str(total_queries), str(successful_queries), str(float(total_queries)/float(total_duration)), str(float(total_queries - successful_queries)/float(total_bad_queries_duration)), str(float(total_queries)/float(state["duration"])) ])

	print "Done :)"
	log_file.close()
	results_file.close()
	