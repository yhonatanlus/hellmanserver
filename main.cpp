#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>
#include <chrono>
#include <cmath>
#include <unistd.h>
#include <malloc.h>
#include <sstream>
#include <stdexcept>
#include <string>
#include <pthread.h>
#include "HellmanTable.h"

#define DEBUG_FUNC 0 
#include "MD5.h" 

#define MB 1048576
#define KB 1024
#define B 1
// CONFIGURATION// 

#define SERVER_PORT 12345
#define MAX_CONN 1000   // The maximum number of clients allowed - Can be changed
#define MAIN_DEBUG 1

using namespace std::chrono;
using namespace RaaS;

const unsigned int DEF_INIT_SEED = 0;
const std::string DEF_SAVE_PATH = ".";
const std::string DEF_FILE_NAME = "tables.bin";

struct client_thread_args {
	int conn_fd; 
	HellmanTable* table;
};

//Global Variables
const len_t DEFAULT_CHAIN_LENGTH = 1024; //Default length of chains.

//Returns resident set size of current process
uint64_t memusage(){
    char line[128];
    /* Report memory usage from /proc/self/status interface. */
    FILE* status = fopen("/proc/self/status", "r");
    if (!status)
        return 0;

    long long unsigned VmRSS = 0;

    while (!feof(status)) {
        fgets(line, 128, status);
        if (strncmp(line, "VmRSS:", 6) == 0){
            if (sscanf(line, "VmRSS: ""%llu", &VmRSS) != 1) {
                fclose(status);
                return 0;
            }
            break;
        }
    }
    fclose(status);
    return VmRSS;
}

// This threads handles the sub tables loading
void *mt_table_handeling_thread(void *table){
	((HellmanTable*)table)->mt_handle_tables(DEF_FILE_NAME);
	pthread_exit(0);
}

// New client routine
void *client_handler(void *args)
{
	char recv_buf[64];
	char* ptr;
	memset(recv_buf, 0x00, 64);
	int bytes_recieved;
	
	int client_sock = (((struct client_thread_args*)args)->conn_fd);
	HellmanTable* table = ((struct client_thread_args*)args)->table;

	if ((bytes_recieved = read(client_sock, recv_buf, sizeof(recv_buf) - 1)) <= 0){
		if (bytes_recieved <0 )
			perror("Client read error occured\n");	
		else
			cout << "Client Disconnected\n" << endl;	
	}
	// Terminate the buffer
	recv_buf[bytes_recieved] = '\0';
	
	// In case this is a query to change the memory
	if((ptr = strstr(recv_buf, "change_mem")) != 0 ){
		if (table->mt_update_new_mem(atoi((ptr+ 11))))
			write(client_sock, "OK", strlen("OK"));
		else
			perror("Something is not ok");
	}
	// In case it is a client that send queries to resolve
	else if( strcmp(recv_buf, "new_client") == 0){
		write(client_sock, "OK", strlen("OK"));
		table->mt_get_preimages(client_sock);
	}
	// All other
	else{
		perror("Unsupported client type\n");	
	}

	// Cleanup
	close(client_sock);
	free(args);
	// pthread_exit(0); // change to return?
}


int main(int argc, char** argv){ // for the future - set flags for setting from initial cmdline args
	srand(DEF_INIT_SEED); // setting the seed for rand
	HellmanTable table(DEFAULT_CHAIN_LENGTH,DEF_SAVE_PATH);
	
	struct client_thread_args *args;
	int sock_fd = 0, conn_fd = 0;
	struct sockaddr_in serv_addr;
	pthread_t last_thread;
	
	cout << "Starting Hellman Server" << endl;
	
	// This thread will handle the table loading and deleting
	if(pthread_create(&last_thread, NULL, &mt_table_handeling_thread, &table) < 0)
	{
		perror("Error creating a table handeling thread");
		exit(0);
	}
	
	// Intializing Server
	try {
		sock_fd = socket(AF_INET, SOCK_STREAM, 0);
		memset(&serv_addr, '0', sizeof(serv_addr));
	
		serv_addr.sin_family = AF_INET;
		serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
		serv_addr.sin_port = htons(SERVER_PORT);
	
		bind(sock_fd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
		listen(sock_fd, MAX_CONN);
	}
	// Error occured while intializing the server
	catch(const exception& e){
		close(sock_fd);
		cout << "Could not start Hellman server" << endl;
		cerr << e.what() << endl;
		exit(0);
	}
	
	cout << "Helmman server started!" << endl;
	
	// Listening for new clients
	while(true) {
		conn_fd = accept(sock_fd, (struct sockaddr*)NULL, NULL);
		args = (struct client_thread_args *)malloc(sizeof(struct client_thread_args));
		args->conn_fd = conn_fd;
		args->table = &table;
		
		
		if(pthread_create(&last_thread, NULL, client_handler, (void*) args) < 0)
		{
			perror("Error creating a new thread");
			break;
		}
		pthread_detach(last_thread); // For the thread resources to be released
		
		cout << "New Connection" << endl;
	}
		
	return 0;
}
